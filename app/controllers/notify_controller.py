from flask import jsonify, make_response
from app.controllers import controller_bp as bp
from app.func.notify import NotifyAPI


@bp.route('/notify/datepick', methods=['POST'])
def get_borrower_by_datepicker():
    return make_response(jsonify(NotifyAPI.datepicker()))


@bp.route('/notify/rangepick', methods=['POST'])
def get_borrower_by_rangepicker():
    return make_response(jsonify(NotifyAPI.rangepicker()))


@bp.route('/email', methods=['POST'])
def send_message():
    return make_response(jsonify(NotifyAPI.send_message()))
