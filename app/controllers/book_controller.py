from flask import jsonify, make_response
from app.controllers import controller_bp as bp
from app.models.book import Book


@bp.route('/books', methods=['GET'])
def get_all_books():
    return make_response(jsonify(Book.get_all_books()))


@bp.route('/books/name:<name>', methods=['GET'])
def get_books_by_name(name):
    return make_response(jsonify(Book.get_books_by_name(name)))


@bp.route('/books/author:<author>', methods=['GET'])
def get_books_by_author(author):
    return make_response(jsonify(Book.get_books_by_author(author)))


@bp.route('/books/category:<category>', methods=['GET'])
def get_books_by_category(category):
    return make_response(jsonify(Book.get_books_by_category(category)))


@bp.route('/books/ocr', methods=['POST'])
def ocr_search():
    return make_response(jsonify(Book.get_books_by_ocr()))
