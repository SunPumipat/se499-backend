from flask import Blueprint

controller_bp = Blueprint('controllers',__name__)

from app.controllers import book_controller
from app.controllers import reserve_controller
from app.controllers import member_controller
from app.controllers import auth_controller
from app.controllers import notify_controller
from app.controllers import queue_controller