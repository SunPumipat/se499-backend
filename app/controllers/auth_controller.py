from flask import jsonify, make_response
from app.controllers import controller_bp as bp
from app.models.member import Member
from app.func.auth import AuthAPI


@bp.route('/ensure', methods=['GET'])
def ensure_token():
    return make_response(jsonify(AuthAPI.ensure_token()))


@bp.route('/login', methods=['POST'])
def login_user():
    return make_response(jsonify(AuthAPI.verify_user()))


@bp.route('/logout', methods=['GET'])
def logout_user():
    return make_response(jsonify(AuthAPI.logout_token()))

