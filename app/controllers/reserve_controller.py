from flask import jsonify, make_response
from app.controllers import controller_bp as bp
from app.models.reservation import Reservation


@bp.route('/reserve', methods=['POST'])
def make_reservation():
    return make_response(jsonify(Reservation.insert_reservation()))

@bp.route('/reserve/list', methods=['POST'])
def get_borrowed_list():
    return make_response(jsonify(Reservation.get_borrowed_list()))

@bp.route('/renew', methods=['PUT'])
def make_renew():
    return make_response(jsonify(Reservation.update_reservation()))
