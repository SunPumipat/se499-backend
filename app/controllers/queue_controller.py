from flask import jsonify, make_response
from app.controllers import controller_bp as bp
from app.models.queue import Queue_book


@bp.route('/queue', methods=['POST'])
def make_queue():
    return make_response(jsonify(Queue_book.insert_queue()))

@bp.route('/queue/view', methods=['POST'])
def view_queue_list():
    return make_response(jsonify(Queue_book.get_queue_list()))

@bp.route('/queue/list', methods=['POST'])
def get_queue_by_id():
    return make_response(jsonify(Queue_book.get_queue_by_id()))

@bp.route('/queue/delete', methods=['POST'])
def delete_queue():
    return make_response(jsonify(Queue_book.delete_queue()))

@bp.route('/queue/add', methods=['POST'])
def add_queue_reserve():
    return make_response(jsonify(Queue_book.add_queue_reserve()))

@bp.route('/queue/next', methods=['GET'])
def view_next_queue():
    return make_response(jsonify(Queue_book.view_next_queue()))

@bp.route('/return', methods=['POST'])
def return_reservation():
    return make_response(jsonify(Queue_book.delete_reservation()))
