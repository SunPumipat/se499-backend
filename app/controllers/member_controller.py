from flask import jsonify, make_response
from app.controllers import controller_bp as bp
from app.models.member import Member


@bp.route('/member', methods=['POST'])
def get_member_by_id():
    return make_response(jsonify(Member.get_member_by_id()))

@bp.route('/member/username', methods=['POST'])
def get_member_by_username():
    return make_response(jsonify(Member.get_member_by_username()))

@bp.route('/member/role', methods=['POST'])
def get_role_by_id():
    return make_response(jsonify(Member.get_role_by_id()))

@bp.route('/member/regist', methods=['POST'])
def make_new_user():
    return make_response(jsonify(Member.make_register()))

@bp.route('/member/update', methods=['POST'])
def update_user():
    return make_response(jsonify(Member.update_member()))