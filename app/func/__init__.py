from flask import Blueprint

func_bp = Blueprint('func', __name__)

from app.func import auth
from app.func import notify