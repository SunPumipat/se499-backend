from flask import request, render_template
from flask_mail import Message
from app.controllers.reserve_controller import Reservation
from app.controllers.book_controller import Book
from app.controllers.member_controller import Member
from app import db, mail
from datetime import datetime as dt
from sqlalchemy import func


class NotifyAPI():

    @classmethod
    def to_br_list(cls, x):
        br = {
            'res_id': x.res_id,
            'mem_id': x.mem_id,
            'mem_name': x.mem_name,
            'mem_surname': x.mem_surname,
            'book_name': x.book_name,
            'reserve_date': x.reserve_date,
            'return_date': x.return_date,
        }
        return br

    @classmethod
    def datepicker(cls):
        try:
            posted_date = request.get_json()
            new_posted = dt.strptime(
                posted_date['return_date'], "%a %b %d %Y").date()
            br_list = list(map(NotifyAPI.to_br_list, db.session
                               .query(Reservation.res_id, Reservation.mem_id, Member.mem_name, Member.mem_surname, Book.book_name,
                                      Reservation.reserve_date, Reservation.return_date)
                               .join(Member, Reservation.mem_id == Member.mem_id)
                               .join(Book, Reservation.book_id == Book.book_id)
                               .filter(func.DATE(Reservation.return_date) == new_posted)
                               .all()))
            if not br_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'Borrow books do not exist.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'success',
                    'message': 'Borrow books is coming.',
                    'data': br_list
                }
                return responseObject
        except ValueError as v:
            responseObject = {
                'status': 'value incorrect format',
                'message': str(v)
            }
            return responseObject
        except Exception as e:
            responseObject = {
                'status': 'error',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def rangepicker(cls):
        try:
            posted_date = request.get_json()
            first_posted = dt.strptime(
                posted_date['first'], "%a %b %d %Y").date()
            second_posted = dt.strptime(
                posted_date['second'], "%a %b %d %Y").date()
            br_list = list(map(NotifyAPI.to_br_list, db.session
                               .query(Reservation.res_id, Reservation.mem_id, Member.mem_name, Member.mem_surname, Book.book_name,
                                      Reservation.reserve_date, Reservation.return_date)
                               .join(Member, Reservation.mem_id == Member.mem_id)
                               .join(Book, Reservation.book_id == Book.book_id)
                               .filter(func.DATE(Reservation.return_date) >= first_posted)
                               .filter(func.DATE(Reservation.return_date) <= second_posted)
                               .all()))
            if not br_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'Borrow books do not exist.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'success',
                    'message': 'Borrow books is coming.',
                    'data': br_list
                }
                return responseObject
        except ValueError as v:
            responseObject = {
                'status': 'value incorrect format',
                'message': str(v)
            }
            return responseObject        
        except Exception as e:
            responseObject = {
                'status': 'error',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def send_message(cls):
        try:
            posted_id = request.get_json()
            recipient = Member.get_email_by_id(id=posted_id['mem_id'])
            msg = Message('Annoucement from Library')
            msg.add_recipient(recipient.mem_email)
            msg.html = render_template(
                '/message_notify.html', name=recipient.mem_name, surname=recipient.mem_surname, id=recipient.mem_id)
            mail.send(msg)

            responseObject = {
                'status': 'success',
                'message': 'The system provide the message to user.'
            }
            return responseObject
        except Exception as e:
            responseObject = {
                'status': 'error',
                'message': str(e)
            }
            return responseObject
