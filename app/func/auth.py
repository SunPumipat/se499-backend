import jwt
from flask import request
from datetime import datetime as dt,  timedelta as td
from app import application, bcrypt
from app.models.member import Member
from app.models.bl_token import Blacklist_token


class AuthAPI():
    
    @classmethod
    def ensure_token(cls):
        try:
            got_token = request.headers.get('Authorization')
            if got_token:
                try:
                    auth_token = got_token.split(" ")[1]
                except IndexError:
                    responseObject = {
                        'status': 'fail',
                        'message': 'Bearer token malformed.'
                    }
                    return responseObject
            else:
                auth_token = ''
            if auth_token:
                mem_id_decode = AuthAPI.decode_auth_token(
                    auth_token=auth_token)
                if not isinstance(mem_id_decode, str):
                    member = Member.get_id_to_token(id=mem_id_decode)
                    responseObject = {
                        'status': 'success',
                        'data': member
                    }
                    return responseObject
                responseObject = {
                    'status': 'fail',
                    'message': mem_id_decode
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Provide a valid auth token.'
                }
                return responseObject

        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Try again'
            }
            return responseObject

    @classmethod
    def encode_auth_token(cls, id):
        payload = {
            'exp': dt.utcnow() + td(days=0, seconds=6400),
            'iat': dt.utcnow(),
            'sub': id
        }
        return jwt.encode(
            payload,
            application.config.get('SECRET_KEY'),
            algorithm='HS256'
        )

    @classmethod
    def decode_auth_token(cls, auth_token):
        try:
            payload = jwt.decode(
                auth_token, application.config.get('SECRET_KEY'))
            is_blacklisted_token = Blacklist_token.check_blacklist(
                auth_token=auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

    @classmethod
    def verify_user(cls):
        try:
            posted_login = request.get_json()
            member = Member.query.filter(
                Member.mem_email == posted_login['username']).first()
            if member and bcrypt.check_password_hash(
                    member.set_password_hash(password=member.mem_pass), posted_login['password']):
                auth_token = AuthAPI.encode_auth_token(id=member.mem_id)
                if auth_token:
                    responseObject = {
                        'status': 'success',
                        'message': 'Successfully logged in.',
                        'data': auth_token.decode(encoding='UTF-8', errors='strict')
                    }
                    return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Email or Password is not correct, please try again.'
                }
            return responseObject
        except Exception as e:
            print(e)
            responseObject = {
                'status': 'fail',
                'message': 'Try again'
            }
            return responseObject

    @classmethod
    def logout_token(cls):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            auth_token = auth_header.split(" ")[1]
        else:
            auth_token = ''
        if auth_token:
            mem_id_decode = AuthAPI.decode_auth_token(auth_token=auth_token)
            if not isinstance(mem_id_decode, str):
                blacklist_token = Blacklist_token()
                blacklist_token.to_insert(token=auth_token)
                try:
                    blacklist_token.save_to_db()
                    responseObject = {
                        'status': 'success',
                        'message': 'Successfully logged out.'
                    }
                    return responseObject
                except Exception as e:
                    responseObject = {
                        'status': 'fail',
                        'message': e
                    }
                    return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': mem_id_decode
                }
                return responseObject
        else:
            responseObject = {
                'status': 'fail',
                'message': 'Provide a valid auth token.'
            }
            return responseObject
