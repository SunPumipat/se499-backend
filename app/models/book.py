from flask import jsonify, request
from app import db
import pymysql
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer
import os
try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract
# from app import mysql
# Dict data type will come from <Class>.query.filter(<any condition>)
# Class object will come from <Class>.query.filter(<any condition>).first() and .all()

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
UPLOAD_FOLDER = '/app/static/uploads/'


class Book(db.Model):
    __tablename__ = 'book'
    book_id = db.Column(db.String(45), primary_key=True)
    book_name = db.Column(db.String(255), nullable=False)
    book_author = db.Column(db.String(255), nullable=False)
    book_category = db.Column(db.String(255), nullable=False)
    book_status = db.Column(db.Boolean, nullable=False)
    reservations = db.relationship(
        'Reservation', backref='book', lazy='dynamic')

    @classmethod
    def to_list(cls, x):
        books = {
            'book_id': x.book_id,
            'book_name': x.book_name,
            'book_author': x.book_author,
            'book_category': x.book_category,
            'book_status': x.book_status
        }
        return books

    @classmethod
    def get_all_books(cls):
        try:
            book_list = list(map(Book.to_list, Book.query.all()))
            return book_list

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': e
            }
            return responseObject

    @classmethod
    def get_books_by_ocr(cls):
        try:
            file = request.files['file']
            extracted_text = Book.upload_image(file)
            extracted_text = extracted_text.replace("\r", " ")
            extracted_text = extracted_text.replace("\n", " ")
            book_has_score = Book.get_similar_book(extracted_text)
            book_list = []
            for i in range(0, len(book_has_score)):
                book_list.extend(list(map(Book.to_list, Book.query.filter(
                    Book.book_id.like(book_has_score[i])))))
            # book_list = list(
            #     map(Book.to_list, Book.query.filter(Book.book_name.like(data))))

            if not book_list:
                responseObject = {
                    'status': 'empty',
                    'message': extracted_text + ': not have this book. '
                }
                return responseObject
            else:
                for book in book_list:
                    if book['book_status'] == 1:
                        book['book_status'] = "On the Shelves"
                    else:
                        book['book_status'] = "Borrowed"

                responseObject = {
                    'status': 'success',
                    'message': extracted_text,
                    'data': book_list
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def get_similar_book(cls, text):
        try:
            sqlEngine = create_engine(
                'mysql://root:211140san@localhost/test', pool_recycle=3600)
            dbConnection = sqlEngine.connect()
            books = pd.read_sql("SELECT * FROM test.book", dbConnection)
            score = pd.DataFrame(
                columns=['book_id', 'book_name', 'similar_score'])
            score['book_name'] = books['book_name']
            score['book_id'] = books['book_id']
            dbConnection.close()

            for i in range(0, len(books)):
                score['similar_score'][i] = Book.similar_score(
                    text, books['book_name'][i])

            list_books_id = Book.get_book_has_score(score)
            return list_books_id
        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def similar_score(cls, text, book_name):
        try:
            l1 = []
            l2 = []
            trash_words = [')', '(', '&', ' ', ':', '[', ']']
            sw = stopwords.words('english')
            a_list = word_tokenize(text.lower())
            b_list = word_tokenize(book_name.lower())

            a_set = {w for w in a_list if not w in sw and not w in trash_words}
            b_set = {w for w in b_list if not w in sw and not w in trash_words}

            rvector = a_set.union(b_set)
            for w in rvector:
                if w in a_set:
                    l1.append(1)  # create a vector
                else:
                    l1.append(0)
                if w in b_set:
                    l2.append(1)
                else:
                    l2.append(0)
            c = 0

            for i in range(len(rvector)):
                c += l1[i]*l2[i]
            cosine = c / float((sum(l1)*sum(l2))**0.5)

            return cosine
        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def get_book_has_score(cls, df):
        top_book = []
        for i in range(0, len(df)):
            if df['similar_score'][i] != 0:
                top_book.append(df['book_id'][i])
        return top_book

    @classmethod
    def get_books_by_name(cls, name):
        try:
            data = "%" + name + "%"
            book_list = list(
                map(Book.to_list, Book.query.filter(Book.book_name.like(data))))

            if not book_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'Books do not exist.'
                }
                return responseObject
            else:
                for book in book_list:
                    if book['book_status'] == 1:
                        book['book_status'] = "On the Shelves"
                    else:
                        book['book_status'] = "Borrowed"

                responseObject = {
                    'status': 'success',
                    'message': 'Books is coming.',
                    'data': book_list
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def get_books_by_author(cls, author):
        try:
            data = "%" + author + "%"
            book_list = list(
                map(Book.to_list, Book.query.filter(Book.book_author.like(data))))

            if not book_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'Books do not exist.'
                }
                return responseObject
            else:
                for book in book_list:
                    if book['book_status'] == 1:
                        book['book_status'] = "On the Shelves"
                    else:
                        book['book_status'] = "Borrowed"

                responseObject = {
                    'status': 'success',
                    'message': 'Books is coming.',
                    'data': book_list
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def get_books_by_category(cls, category):

        try:
            data = "%" + category + "%"
            book_list = list(
                map(Book.to_list, Book.query.filter(Book.book_category.like(data))))

            if not book_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'Books do not exist.'
                }
                return responseObject
            else:
                for book in book_list:
                    if book['book_status'] == 1:
                        book['book_status'] = "On the Shelves"
                    else:
                        book['book_status'] = "Borrowed"

                responseObject = {
                    'status': 'success',
                    'message': 'Books is coming.',
                    'data': book_list
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def update_book_status(cls, id, mode):
        try:
            if mode == "renew":
                book = Book.query.filter(Book.book_id == id)
                dataToUpdate = {Book.book_status: False}
                book.update(dataToUpdate)
               
            elif mode == "return":
                book = Book.query.filter(Book.book_id == id)
                dataToUpdate = {Book.book_status: True}
                book.update(dataToUpdate)
                
            db.session.commit()
        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def upload_image(cls, file):
        if file and Book.allowed_file(file.filename):
            file.save(os.path.join(os.getcwd() + UPLOAD_FOLDER, file.filename))

            # call the OCR function on it
            extracted_text = Book.ocr_core(file)
            return extracted_text

    @classmethod
    def allowed_file(cls, filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    @classmethod
    def ocr_core(cls, filename):
        """
        This function will handle the core OCR processing of images.
        """
        pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
        text = pytesseract.image_to_string(Image.open(
            filename))  # We'll use Pillow's Image class to open the image and pytesseract to detect the string in the image
        return text  # Then we will print the text in the image
