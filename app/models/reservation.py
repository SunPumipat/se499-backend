from flask import jsonify, redirect, request, flash
from app import db
from datetime import datetime as dt, timedelta as add
from uuid import uuid4
from app.models.book import Book
from app.models.bl_reservation import Blacklist_Reservation

# List of Object will come from <Class>.query.filter(<any condition>)
# List of Object will come from <Class>.query.filter(<any condition>).all()
# Class object will come from <Class>.query.filter(<any condition>).first()


class Reservation(db.Model):
    __tablename__ = 'reservation'
    res_id = db.Column(db.Integer, primary_key=True,
                       unique=True, autoincrement=True)
    book_id = db.Column(db.String(45), db.ForeignKey(
        'book.book_id'), nullable=False)
    mem_id = db.Column(db.Integer, db.ForeignKey(
        'member.mem_id'), nullable=False)
    reserve_date = db.Column(db.DateTime, nullable=False)
    return_date = db.Column(db.DateTime, nullable=False)
    renew_status = db.Column(db.Boolean, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_in_db(self):
        db.session.delete(self)
        db.session.commit()

    def to_insert(self, x):
        self.book_id = x['book_id']
        self.mem_id = x['mem_id']
        self.reserve_date = dt.now().strftime("%Y-%m-%d %H:%M:%S")
        self.return_date = (dt.now() + add(days=7)
                            ).strftime("%Y-%m-%d %H:%M:%S")
        self.renew_status = True

    @classmethod
    def to_br_list(cls, x):
        br = {
            'res_id': x.res_id,
            'book_name': x.book_name,
            'reserve_date': x.reserve_date,
            'return_date': x.return_date,
            'renew_status': x.renew_status
        }
        return br

    @classmethod
    def insert_reservation(cls):
        try:
            posted_reserve = request.get_json()
            new_rs = Reservation()
            new_rs.to_insert(posted_reserve)
            # add to database
            new_rs.save_to_db()
            Book.update_book_status(posted_reserve['book_id'], "renew")
            rs_object = Reservation.query.filter(
                Reservation.res_id == new_rs.res_id).first()
            if rs_object.res_id == new_rs.res_id and rs_object.book_id == new_rs.book_id and \
                    rs_object.mem_id == new_rs.mem_id:
                responseObject = {
                    'status': 'success',
                    'message': 'Reservation successful'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Reservation is missing'
                }
                return responseObject

        except Exception as e:
            print(e)

    @classmethod
    def update_reservation(cls):
        try:
            posted_renew = request.get_json()
            rs_list = Reservation.query.filter(
                Reservation.res_id == posted_renew['res_id'])
            rs_object = Reservation.query.filter(
                Reservation.res_id == posted_renew['res_id']).first()
            new_return = (rs_object.return_date + add(days=3)
                          ).strftime("%Y-%m-%d %H:%M:%S")
            dataToUpdate = {Reservation.renew_status: False,
                            Reservation.return_date: new_return}
            rs_list.update(dataToUpdate)
            db.session.commit()

            if rs_object.return_date.strftime("%Y-%m-%d %H:%M:%S") == new_return and rs_object.res_id == posted_renew['res_id']:
                responseObject = {
                    'status': 'success',
                    'message': 'Renew is success.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Renew is failure.'
                }
                return responseObject

        except Exception as e:
            print(e)

    @classmethod
    def get_borrowed_list(cls):
        try:
            posted_id = request.get_json()
            br_list = list(map(Reservation.to_br_list, db.session
                               .query(Reservation.res_id, Book.book_name, Reservation.reserve_date,
                                      Reservation.return_date, Reservation.renew_status)
                               .join(Book, Reservation.book_id == Book.book_id)
                               .filter(Reservation.mem_id == posted_id['mem_id'])))

            if not br_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'Borrow books do not exist.'
                }
                return responseObject
            else:
                for br in br_list:
                    if br['renew_status'] != 0:
                        br['renew_status'] = 'Available'
                    else:
                        br['renew_status'] = 'Not Available'
                responseObject = {
                    'status': 'success',
                    'message': 'Borrowed book is coming.',
                    'data': br_list
                }
                return responseObject

        except Exception as e:
            print(e)

    @classmethod
    def disable_renew(cls, book_id):
        try:
            rs_list = Reservation.query.filter(
            Reservation.book_id == book_id)
            dataToUpdate = {Reservation.renew_status: False}
            rs_list.update(dataToUpdate)
            db.session.commit()

        except Exception as e:
            print(e)