from app import db
from datetime import datetime as dt


class Blacklist_token (db.Model):
    __tablename__ = 'blacklist_token'
    bl_id = db.Column(db.Integer, primary_key=True,
                      autoincrement=True, nullable=False)
    bl_token = db.Column(db.String(500), unique=True, nullable=False)
    bl_on = db.Column(db.DateTime, nullable=False)

    def to_insert(self, token):
        self.bl_token = token
        self.bl_on = dt.now()
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def check_blacklist(auth_token):
        # auth_token is string
        res = Blacklist_token.query.filter(
            Blacklist_token.bl_token == auth_token).first()
        if res:
            return True
        else:
            return False
            
    
    