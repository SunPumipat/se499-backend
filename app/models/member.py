from flask import request
from app import application, db, bcrypt
import jwt
from datetime import datetime as dt,  timedelta as td


class Member(db.Model):
    __tablename__ = 'member'
    mem_id = db.Column(db.Integer, primary_key=True, unique=True)
    mem_name = db.Column(db.String(45), nullable=False)
    mem_surname = db.Column(db.String(45), nullable=False)
    mem_type = db.Column(db.String(45), nullable=False)
    mem_email = db.Column(db.String(45), nullable=False, unique=True)
    mem_pass = db.Column(db.String(45), nullable=False)
    description = db.Column(db.String(45))
    reservations = db.relationship(
        "Reservation", backref='member', lazy='dynamic')

    @classmethod
    def to_list(cls, x):
        member = {
            'mem_id': x.mem_id,
            'mem_name': x.mem_name,
            'mem_surname': x.mem_surname,
            'mem_type': x.mem_type,
            'mem_email': x.mem_email,
            'mem_pass': x.mem_pass,
            'description': x.description
        }
        return member

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def to_insert(self, x):
        self.description = x['description']
        self.mem_email = x['mem_email']
        self.mem_pass = x['mem_pass']
        self.mem_name = x['mem_name']
        self.mem_surname = x['mem_surname']
        self.mem_type = x['mem_type']
        self.mem_id = x['mem_id']

    @classmethod
    def update_member(cls):
        try:
            posted = request.get_json()
            target_member = Member.query.filter(
                Member.mem_id == posted['mem_id'])
            new_info = {
                Member.description: posted['description'],
                Member.mem_email: posted['mem_email'],
                Member.mem_name: posted['mem_name'],
                Member.mem_surname: posted['mem_surname'],
                Member.mem_pass: posted['mem_pass']
            }
            target_member.update(new_info)
            db.session.commit()

            if Member.query.filter(Member.mem_email == posted['mem_email']).first():
                responseObject = {
                    'status': 'success',
                    'message': 'User information has been updated'
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)
            }
            return responseObject

    @classmethod
    def make_register(cls):
        try:
            posted = request.get_json()
            new_member = Member()
            new_member.to_insert(posted)
            new_member.save_to_db()
            isDone_user = Member.query.filter(
                Member.mem_id == posted['mem_id']).first()
            if isDone_user:
                responseObject = {
                    'status': 'success',
                    'message': 'success to create new user.'
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': 'duplicate id or mail'
            }
            return responseObject

    @classmethod
    def get_id_to_token(cls, id):
        try:
            member = Member.query.filter(Member.mem_id == id).first()
            if not member:
                responseObject = {
                    'status': 'fail',
                    'message': 'Can not retrieve information.'
                }
                return responseObject
            else:
                return member.mem_id

        except Exception as e:
            print(e)

    @classmethod
    def get_member_by_id(cls):
        try:
            posted_id = request.get_json()
            member = list(
                map(Member.to_list, Member.query.filter(Member.mem_id == posted_id['mem_id'])))
            if not member:
                responseObject = {
                    'status': 'fail',
                    'message': 'Can not retrieve information.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'success',
                    'message': 'Member is coming.',
                    'data': member
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)

            }
            return responseObject

    @classmethod
    def get_member_by_username(cls):
        try:
            posted = request.get_json()
            member = list(
                map(Member.to_list, Member.query.filter(Member.mem_email == posted['username'])))
            if not member:
                responseObject = {
                    'status': 'fail',
                    'message': 'Can not retrieve information.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'success',
                    'message': 'Member is coming.',
                    'data': member
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)

            }
            return responseObject

    @classmethod
    def get_role_by_id(cls):
        try:
            posted_id = request.get_json()
            member = Member.query.filter(
                Member.mem_id == posted_id['mem_id']).first()
            if not member:
                responseObject = {
                    'status': 'fail',
                    'message': 'Can not retrieve information.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'success',
                    'message': 'Role is coming.',
                    'data': member.mem_type
                }
                return responseObject

        except Exception as e:
            responseObject = {
                'status': 'fail',
                'message': str(e)

            }
            return responseObject

    @classmethod
    def get_email_by_id(cls, id):
        try:
            member = Member.query.filter(Member.mem_id == id).first()
            if not member:
                responseObject = {
                    'status': 'fail',
                    'message': 'Can not retrieve information.'
                }
                return responseObject
            else:
                return member
        except Exception as e:
            print(e)

    def set_password_hash(self, password):
        return bcrypt.generate_password_hash(password, application.config.get('BCRYPT_LOG_ROUNDS'))
