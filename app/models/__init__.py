from flask import Blueprint

model_bp = Blueprint('models', __name__)

from app.models import book
from app.models import reservation
from app.models import member
from app.models import bl_token
from app.models import bl_reservation
from app.models import queue