from app import db
from datetime import datetime as dt, timedelta as add
from flask import request
from app.models.reservation import Reservation
from app.models.book import Book
from app.models.member import Member
from app.models.bl_reservation import Blacklist_Reservation


class Queue_book(db.Model):
    __tablename__ = 'queue'
    queue_id = db.Column(db.Integer, primary_key=True,
                         unique=True, autoincrement=True)
    qbook_id = db.Column(db.String(45),  db.ForeignKey(
        'book.book_id'), nullable=False)
    qmem_id = db.Column(db.Integer, db.ForeignKey(
        'member.mem_id'), nullable=False)
    qon_time = db.Column(db.DateTime, nullable=False)

    def delete_in_db(self):
        db.session.delete(self)
        db.session.commit()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def to_insert(self, x):
        self.qbook_id = x['book_id']
        self.qmem_id = x['mem_id']
        self.qon_time = dt.now().strftime("%Y-%m-%d %H:%M:%S")

    @classmethod
    def to_queue_list(cls, x):
        q_list = {
            'queue_id': x.queue_id,
            'qbook_id': x.qbook_id,
            'qbook_name': x.book_name,
            'qmem_id': x.qmem_id,
            'qmem_fullname': x.mem_name + " " + x.mem_surname,
            'qon_time': x.qon_time
        }
        return q_list

    @classmethod
    def insert_queue(cls):
        try:
            posted = request.get_json()
            if Queue_book.check_able_queue(posted):
                new_queue = Queue_book()
                new_queue.to_insert(posted)
                new_queue.save_to_db()
                Reservation.disable_renew(posted['book_id'])
                has_queue = Queue_book.query.filter(
                    Queue_book.qbook_id == posted['book_id']).first()
                if has_queue:
                    responseObject = {
                        'status': 'success',
                        'message': 'Queue book success'
                    }
                    return responseObject
                else:
                    responseObject = {
                        'status': 'fail',
                        'message': 'Queue book failure'
                    }
                return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'You have this book in queue or in the reservation list '
                }
                return responseObject
        except Exception as e:
            print(e)

    @classmethod
    def check_able_queue(cls, posted):
        in_queue = Queue_book.query.filter(
            Queue_book.qbook_id == posted['book_id'], Queue_book.qmem_id == posted['mem_id']).first()
        in_reserve = Reservation.query.filter(
            Reservation.book_id == posted['book_id'], Reservation.mem_id == posted['mem_id']).first()
        if in_queue or in_reserve:
            return False
        else:
            return True

    @classmethod
    def get_queue_list(cls):
        try:
            posted = request.get_json()
            q_only_book = db.session.query(Queue_book.qbook_id).filter(
                Queue_book.qmem_id == posted['mem_id'])
            q_list = []
            for i in q_only_book:
                q_list.extend(list(map(Queue_book.to_queue_list, db.session
                                       .query(Queue_book.queue_id, Queue_book.qbook_id, Book.book_name, Queue_book.qmem_id,
                                              Member.mem_name, Member.mem_surname, Queue_book.qon_time)
                                       .join(Book, Queue_book.qbook_id == Book.book_id)
                                       .join(Member, Queue_book.qmem_id == Member.mem_id)
                                       .filter(Queue_book.qbook_id == i.qbook_id))))
            if not q_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'Request do not exist.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'success',
                    'message': 'Request is coming.',
                    'data': q_list
                }
                return responseObject

        except Exception as e:
            print(e)

    @classmethod
    def get_queue_by_id(cls):
        try:
            posted = request.get_json()
            q_list = list(map(Queue_book.to_queue_list, db.session
                              .query(Queue_book.queue_id, Queue_book.qbook_id, Book.book_name, Queue_book.qmem_id,
                                     Member.mem_name, Member.mem_surname, Queue_book.qon_time)
                              .join(Book, Queue_book.qbook_id == Book.book_id)
                              .join(Member, Queue_book.qmem_id == Member.mem_id)
                              .filter(Queue_book.qmem_id == posted['mem_id'])))
            if not q_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'Request do not exist.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'success',
                    'message': 'Request is coming.',
                    'data': q_list
                }
                return responseObject

        except Exception as e:
            print(e)

    @classmethod
    def delete_queue(cls):
        try:
            posted = request.get_json()
            delete_queue = Queue_book.query.filter(
                Queue_book.queue_id == posted['queue_id'], Queue_book.qmem_id == posted['mem_id']).first()
            delete_queue.delete_in_db()
            q_list = Queue_book.query.filter(
                Queue_book.queue_id == posted['queue_id'], Queue_book.qmem_id == posted['mem_id']).first()
            if not q_list:
                responseObject = {
                    'status': 'success',
                    'message': 'Delete queue success'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Delete queue failure'
                }
                return responseObject

        except Exception as e:
            print(e)

    @classmethod
    def add_queue_reserve(cls):
        try:
            posted = request.get_json()
            add_reserve = Reservation()
            add_reserve.to_insert(posted)
            add_reserve.save_to_db()
            Book.update_book_status(posted['book_id'], "renew")
            delete_queue = Queue_book.query.filter(
                Queue_book.queue_id == posted['queue_id']).first()
            delete_queue.delete_in_db()

            has_next_queue = Queue_book.query.filter(
                Queue_book.qbook_id == posted['book_id']).first()
            if has_next_queue:
                Reservation.disable_renew(posted['book_id'])

            in_queue = Queue_book.query.filter(
                Queue_book.queue_id == posted['queue_id']).first()
            in_reserve = Reservation.query.filter(
                Reservation.book_id == posted['book_id'], Reservation.mem_id == posted['mem_id']).first()

            if (not in_queue) and in_reserve:
                responseObject = {
                    'status': 'success',
                    'message': 'Add request to reserve success.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Add request to reserve failure.'
                }
                return responseObject

        except Exception as e:
            print(e)

    @classmethod
    def view_next_queue(cls):
        try:
            q_only_book = db.session.query(Queue_book.qbook_id.distinct().label(
                "qbook_id")).order_by(Queue_book.qbook_id)
            q_list = []
            for i in q_only_book:
                queue = db.session.query(Queue_book.queue_id, Queue_book.qbook_id, Book.book_name, Queue_book.qmem_id,
                                         Member.mem_name, Member.mem_surname, Queue_book.qon_time).join(
                    Book, Queue_book.qbook_id == Book.book_id).join(
                        Member, Queue_book.qmem_id == Member.mem_id).filter(Queue_book.qbook_id == i.qbook_id).first()

                in_reserve = Reservation.query.filter(
                    Reservation.book_id == queue.qbook_id).first()
                if not in_reserve:
                    mock_list = {
                        'queue_id': queue.queue_id,
                        'qbook_id': queue.qbook_id,
                        'qbook_name': queue.book_name,
                        'qmem_id': queue.qmem_id,
                        'qmem_fullname': queue.mem_name + " " + queue.mem_surname,
                        'qon_time': queue.qon_time
                    }
                    q_list.append(mock_list)

            if not q_list:
                responseObject = {
                    'status': 'empty',
                    'message': 'No request can not access.'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'success',
                    'message': 'Request can access.',
                    'data': q_list
                }
                return responseObject

        except Exception as e:
            print(e)

    @classmethod
    def delete_reservation(cls):
        try:
            posted = request.get_json()
            delete_book = Reservation.query.filter(
                Reservation.res_id == posted['res_id']).first()
            in_queue = Queue_book.query.filter(
                Queue_book.qbook_id == delete_book.book_id).first()

            if not in_queue:
                Book.update_book_status(delete_book.book_id, "return")
                                
            Blacklist_Reservation.insert_blacklist_reservation(delete_book)
            delete_book.delete_in_db()
            br_list = br_list = list(map(Reservation.to_br_list, db.session
                                         .query(Reservation.res_id, Book.book_name, Reservation.reserve_date,
                                                Reservation.return_date, Reservation.renew_status)
                                         .join(Book, Reservation.book_id == Book.book_id)
                                         .filter(Reservation.res_id == posted['res_id'])))
            if not br_list:
                responseObject = {
                    'status': 'success',
                    'message': 'Return book success'
                }
                return responseObject
            else:
                responseObject = {
                    'status': 'fail',
                    'message': 'Return book failure'
                }
                return responseObject

        except Exception as e:
            print(e)
