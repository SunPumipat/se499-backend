from app import db
from datetime import datetime as dt, timedelta as add


class Blacklist_Reservation(db.Model):
    __tablename__ = 'blacklist_reservation'
    bl_id = db.Column(db.Integer, primary_key=True,
                      unique=True, autoincrement=True)
    blres_id = db.Column(db.Integer, unique=True)
    blbook_id = db.Column(db.String(45), nullable=False)
    blmem_id = db.Column(db.Integer, nullable=False)
    reserve_date = db.Column(db.DateTime, nullable=False)
    return_date = db.Column(db.DateTime, nullable=False)
    renew_status = db.Column(db.Boolean, nullable=False)
    bl_on = db.Column(db.DateTime, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def to_insert(self, x):
        self.blbook_id = x.book_id
        self.blmem_id = x.mem_id
        self.blres_id = x.res_id
        self.reserve_date = x.reserve_date
        self.return_date = x.return_date
        self.renew_status = x.renew_status
        self.bl_on = dt.now().strftime("%Y-%m-%d %H:%M:%S")

    @classmethod
    def insert_blacklist_reservation(cls, deletebook):
        try:
            bl_book = Blacklist_Reservation()
            bl_book.to_insert(deletebook)
            bl_book.save_to_db()

        except Exception as e:
            print(e)