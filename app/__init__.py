from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from flask_mail import Mail

application = Flask(__name__)
db = SQLAlchemy()
migrate = Migrate()
bcrypt = Bcrypt()
mail = Mail()


def create_server():
    config_server(application)
    blueprint_server(application)
    db.init_app(application)
    migrate.init_app(application, db)
    bcrypt.init_app(application)
    mail.init_app(application)
    CORS(application)
    return application

def blueprint_server(application):
    from app.controllers import controller_bp
    from app.models import model_bp
    from app.func import func_bp
    application.register_blueprint(controller_bp)
    application.register_blueprint(model_bp)
    application.register_blueprint(func_bp)

def config_server(application):
    application.config['TESTING'] = False
    application.config['DEBUG'] = True
    application.config['SECRET_KEY'] = 'mykey'
    application.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:211140san@localhost/test'
    application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    mail_config(application)

def mail_config(application):
    application.config['MAIL_SERVER'] = 'smtp.gmail.com'
    application.config['MAIL_PORT'] = 465
    application.config['MAIL_USE_TLS'] = False
    application.config['MAIL_USE_SSL'] = True
    application.config['MAIL_USERNAME'] = 'library.test.sun@gmail.com'
    application.config['MAIL_PASSWORD'] = 'librarysun'
    application.config['MAIL_DEFAULT_SENDER'] = 'library.test.sun@gmail.com'
    application.config['MAIL_MAX_EMAILS'] = None
    application.config['MAIL_ASCII_ATTACHMENTS'] = False
    application.config['MAIL_SUPPRESS_SEND'] = False
